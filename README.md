# Ejemplos para practicar useState

En este proyecto podéis encontrar algunos componentes que podéis intentar hacer
para practicar el uso de useState en React.

Os recomiendo intentar hacerlos con algo de estilo, ya que combinar las dos
cosas es algo que os hará falta en vuestro proyecto, y en vuestro futuro
profesional.

## Cómo probarlos

Como siempre, clonad este repo, y haced:

> npm install  
> npm start

Os recomiendo intentarlos vosotros antes de ver el código fuente.

## Otras ideas

Hay infinidad de otros ejemplos que podéis intentar, y os recomiendo que toméis
inspiración en vuestros propios proyectos. Pero si no se os ocurre nada,
os dejo algunas propuestas más:

- Menú hamburguesa que cubra la página y se pueda cerrar
- Semáforo que alterne entre los colores a cada click
- Personaje (sprite) que se gire en la dirección de la tecla que pulses
  (cambiando entre 4 imagenes; foco en un input para eventos...)
- Lista de palabras que se pueden clicar para formar frases ("Duolinguo")
- Tres en raya
- Cesta de la compra con productos de un JSON y cantidades
- Configurador de reservas de hotel (nº de habitaciones y personas en cada una)
- ...
